package com.cab404.guidy.tests;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.GameScene;
import com.cab404.defense.scene.Scene;
import com.cab404.defense.ui.DelimeterField;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.ui.SelectableField;
import com.cab404.defense.util.FL;
import com.cab404.guidy.Luna;
import com.cab404.guidy.tests.circles.Circles;
import com.cab404.guidy.tests.cosmic.Cosmos;
import com.cab404.guidy.tests.flames.Flame;
import com.cab404.guidy.tests.fluttershy.Cottage;
import com.cab404.guidy.tests.platformer.Platformer;

import java.util.Random;

/**
 * = === === === === === === === === === === === === === === === ===<br>
 * ===   Тут всякие тесты и заголовок в красивом обрамлении    === ===<br>
 * = ===  Не знаю, как вам, а мне нравится.                   == === ===<br>
 * ===                                                         === ===<br>
 * = === === === === === === === === === === === === === === === ===<br>
 *
 * @author cab404
 */
public class MainMenu extends GameScene {

    Music bgm;
    ListMenu menu = new ListMenu();

    @Override
    public void create() {
        super.create();

        FL.getInstance().reloadFont(Gdx.files.internal("data/fonts/ubuntu-mono.ttf"), FL.DEFAULT, 16);

        bgm = Gdx.audio.newMusic(Gdx.files.internal("data/music/mlp.ogg"));
        bgm.setLooping(true);
        bgm.setVolume(0.3f);
//        bgm.play();
        Texture.setEnforcePotImages(false);
        setMain();
        objects.add(menu);

    }

    Vector2 cam = new Vector2();

    public void setMain() {
        menu.killall();
        menu.add(new SelectableField() {
            @Override
            public void onSelect(Vector2 mouse) {
                selMode();
            }

            @Override
            public String getText() {
                return "Новая игра";
            }
        });
        menu.add(new SelectableField() {
            @Override
            public void onSelect(Vector2 mouse) {
                Luna.scene.changeScene(new Circles());
            }

            @Override
            public String getText() {
                return "Круги";
            }
        });
        menu.add(new SelectableField() {
            @Override
            public void onSelect(Vector2 mouse) {
                Luna.scene.changeScene(new Flame());
            }

            @Override
            public String getText() {
                return "Огонь";
            }
        });
        menu.add(new SelectableField() {
            @Override
            public void onSelect(Vector2 mouse) {
                Luna.scene.changeScene(new Platformer());
            }

            @Override
            public String getText() {
                return "Платформер";
            }
        });
        menu.add(new SelectableField() {
            @Override
            public void onSelect(Vector2 mouse) {
                Luna.scene.changeScene(new Cosmos());
            }

            @Override
            public String getText() {
                return "Космос";
            }
        });
    }

    public void selMode() {
        menu.killall();
        menu.add(new SelectableField() {
            @Override
            public void onSelect(Vector2 mouse) {
                changeScene(new Cottage());
            }

            @Override
            public String getText() {
                return "Флаттершай";
            }
        });

        menu.add(new DelimeterField(new Vector2(10, 5)));
        menu.add(new SelectableField() {
            @Override
            public void onSelect(Vector2 mouse) {
                setMain();
            }

            @Override
            public String getText() {
                return "В главное меню";
            }
        });

    }

    @Override
    public void update() {
        super.update();
        // Ставим меню в середину
        menu.pos.set(scr.cpy().div(2).sub(menu.size.cpy().div(2)));

    }

    public void changeScene(Scene scene) {
        Luna.scene.changeScene(scene);
        bgm.stop();
    }


    Vector2 scr = new Vector2();

    @Override
    public void resize(Vector2 size) {
        super.resize(size);
        scr = size.cpy();
    }

    Random rnd = new Random();

    public Color getRandomColor() {
        switch (rnd.nextInt(9)) {
            case 0:
                return Color.BLUE;
            case 1:
                return Color.CYAN;
            case 2:
                return Color.DARK_GRAY;
            case 3:
                return Color.GRAY;
            case 4:
                return Color.RED;
            case 5:
                return Color.ORANGE;
            case 6:
                return Color.PINK;
            case 7:
                return Color.GREEN;
            case 8:
                return Color.LIGHT_GRAY;
            default:
                return Color.WHITE;
        }
    }

}
