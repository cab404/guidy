package com.cab404.guidy.tests.platformer;

import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.rpg.top_view.BoundingObject;

/**
 * @author cab404
 */
public abstract class PlatformerObject extends BoundingObject {
    public float G = 0.5f, GF = 0.6f, AF = 0.01f;
    public Vector2 vel = new Vector2();

    private boolean onGround;

    public boolean isOnGround() {
        return onGround;
    }

    @Override public void update(float time) {

        super.update(time);
        vel.y -= G * time;

        vel.x *= (1 - (AF * time));
        vel.y *= (1 - (AF * time));

        if (onGround)
            vel.x *= 1 - (GF * time);

        Vector2 temp = pos.cpy();
        onGround = false;
        move(vel.cpy().mul(time));

        if (pos.x == temp.x)
            vel.x = 0;
        if (pos.y == temp.y)
            vel.y = 0;
    }

    @Override public void onHit(Side side, BoundingObject object) {
        super.onHit(side, object);
        if (side == Side.BOTTOM) onGround = true;
    }
}
