package com.cab404.guidy.tests.platformer;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cab404.defense.scene.SimpleScene;
import com.cab404.guidy.Luna;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.guidy.rpg.top_view.TiledMapWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cab404
 */
public class Platformer extends SimpleScene {
    List<BoundingObject> bb = new ArrayList<>();

    private class Pony extends PlatformerObject {
        protected Sprite sprite;

        private Pony(Sprite sprite) {
            super();
            this.sprite = sprite;
            this.size.set(16, 16);
        }


        @Override public void render(SpriteBatch batch) {
            sprite.setPosition(pos.x, pos.y);
            sprite.setSize(size.x, size.y);
            sprite.draw(batch);
        }
    }

    TiledMapWrapper map;

    @Override public void create() {
        super.create();

        map = new TiledMapWrapper("data/maps/flutterhouse", 32);
        objects.add(map);

        Pony pony = new Pony(Luna.load("data/imgs/chars/cab.png")) {
            @Override public void update(float time) {
                super.update(time);

                if (vel.x > 0) sprite.flip(!sprite.isFlipX(), false);
                if (vel.x < 0) sprite.flip(sprite.isFlipX(), false);

                if (Luna.key(Input.Keys.A))
                    vel.add(-2, 0);
                if (Luna.key(Input.Keys.D))
                    vel.add(2, 0);
                if (Luna.key(Input.Keys.W) && isOnGround())
                    vel.add(0, 10);

            }
        };

        pony.setBoundingObjects(bb);
        pony.pos.set(573, 441);

        objects.add(pony);
        bb.add(map);
        bb.add(pony);

        for (int i = 0; i < 5; i++) {
            Pony box = new Pony(Luna.load("data/imgs/box.png"));
            box.setBoundingObjects(bb);
            box.pos.set(543, 361 + 16 * i);
            bb.add(box);
            box._static = false;
            objects.add(box);
        }

        Luna.CameraLinkParams cam = new Luna.CameraLinkParams();
        cam.bounds = map.getBounds();
        cam.speed = 0.3f;
        cam.target = pony;
        cam.zoom = 2;

        objects.add(Luna.getCameraLink(cam));
    }
}
