package com.cab404.guidy.tests.flames;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.defense.scene.GameScene;
import com.cab404.guidy.Luna;

import java.util.Random;

/**
 * Много летающих горящих крупов.
 *
 * @author cab404
 */
public class Flame extends GameScene {

    Random rnd = new Random();
    Color[][] colors = new Color[][]{
            Luna.array(Color.YELLOW, Color.RED),
            Luna.array(Color.CYAN, Color.BLUE),
            Luna.array(Color.PINK, Color.MAGENTA),
    };

    class PHGameObject extends GameObj {
        // Картинка несчастной поняши.
//        Sprite spr;

        // Скорость этой самой поняши.
        Vector2 vel;

        // Поворот этой поняши, flipY не используется, но пусть будет.
        boolean flipX, flipY;

        // Цвета шлейфа поняши.
        Color primary = Color.YELLOW, secondary = Color.RED;

        PHGameObject(/**Sprite spr*/) {
//            this.spr = spr;
        }

        @Override
        public void render(SpriteBatch batch) {
            // Указываем, куда поняша смотрит.
            flipX = vel.x > 0;
//
//            // Разворачиваем картинку в начальное положение
//            spr.flip(spr.isFlipX(), spr.isFlipY());
//
//            // ... и применяем свои параметры.
//            spr.flip(flipX, flipY);
//
//            // Ставим картинку куда нужно и рисуем.
//            spr.setPosition(pos.x, pos.y);
//            spr.draw(batch);
        }


        float rot = 0;
        @Override
        public void update(float time) {
            // Двигаем поня.
//            pos.add(vel.cpy().mul(time));


            // Прорабатываем столкновения со стенками.
//            if (pos.x < 0) {
//                pos.x = 0;
//                vel.x = -1 * vel.x;
//            }
//            if (pos.x + size.x > SimpleScene.scr.x) {
//                pos.x = SimpleScene.scr.x - size.x;
//                vel.x = -1 * vel.x;
//            }
//            if (pos.y < 0) {
//                pos.y = 0;
//                vel.y = -1 * vel.y;
//            }
//            if (pos.y + size.y > SimpleScene.scr.y) {
//                pos.y = SimpleScene.scr.y - size.y;
//                vel.y = -1 * vel.y;
//            }

            // Создаём шлейф. При 30 частицах/обновление он очешуенен.
            vel.add(Luna.screen().div(2).sub(pos).nor().mul(0.5f));
            for (int i = 0; i != 10; i++) {
                {
//                    rot += vel.y * time / 30;
                    pos.add(vel.cpy().div(10));
//                    pos.set(Luna.screen().div(2).add(new Vector2(vel.x, 0).rotate(rot)));
                }

                // В параметры передаём время жизни частицы.
                FireParticle fire = new FireParticle((float) (0.1f + 0.5f * Math.random()));

                // Отправляем в "рандомном" направлении.
                fire.pos = this.pos.cpy().add(size.cpy().mul(0.5f + (flipX ? -0.25f : 0.25f), 0.25f));

                // Выставляем градиент
                fire.primary = primary;
                fire.secondary = secondary;

                // Шлём в хранилище
                parent.add(fire);
            }

        }
    }

    @Override
    public void create() {
        super.create();
        // Картинка с подопытным.
//        Sprite spr = new Sprite(new Texture("data/imgs/chars/cab.png"));

        for (int i = 0; i != 20; i++) {
            PHGameObject object = new PHGameObject();

            // Находим случайный градиент в списке и применяем его.
            int ind = rnd.nextInt(colors.length);
            object.primary = colors[ind][0];
            object.secondary = colors[ind][1];

            // Ставим подопытного в середину сцены
            object.pos = Luna.screen().cpy().div(2).add(new Vector2(100, 100).mul(rnd.nextFloat() - 0.5f, rnd.nextFloat() - 0.5f));

            // ... и пинаем ...
            object.vel = new Vector2(10f, 10f);
            // ... в случайном направлении ...
            object.vel.mul(rnd.nextFloat(), rnd.nextFloat());
//            object.vel.sub(350, 350);
            // ... и слуайной скоростью.
            object.vel.x += Math.signum(object.vel.x) * 5;
            object.vel.y += Math.signum(object.vel.y) * 5;

            // Выставляем размер и пихаем в хранилище.
//            object.size = new Vector2(16, 16);
            game.add(object);
        }

    }

}
