package com.cab404.guidy.tests.flames;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cab404.defense.objects.GameObj;

/**
 * @author cab404
 */
public abstract class Particle extends GameObj {
    public Color col;
    private static Sprite pix;

    static {
        Pixmap px = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        px.setColor(Color.WHITE);
        px.fill();
        pix = new Sprite(new Texture(px));
    }


    @Override
    public void render(SpriteBatch batch) {
        pix.setPosition(pos.x, pos.y);
        pix.setColor(col);
        pix.draw(batch);
    }

}
