package com.cab404.guidy.tests.flames;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author cab404
 */
public class FireParticle extends Particle {
    private float lifetime, startLifetime;
    public Color primary = Color.WHITE, secondary = Color.BLUE;
    private float update, update_delay = 0.01f;

    public FireParticle(float lifetime) {
        startLifetime = lifetime;
        this.lifetime = lifetime;
    }

    @Override
    public void render(SpriteBatch batch) {
        if (col == null) col = primary;
        super.render(batch);
    }

    @Override
    public void update(float time) {
        lifetime -= time;
        if (lifetime <= 0) {
            isAlive = false;
            return;
        }
        update += time;
        if (update > update_delay) {
            update = 0;
            if (Math.random() > 0.5f) pos.x++;
            else pos.x--;
            if (Math.random() > 0.5f) pos.y++;
            else pos.y--;

            float phase = lifetime / startLifetime;

            Color prt = primary.cpy().mul(phase);
            prt.add(secondary.cpy().mul(1 - phase));

            if (phase < 0.2)
                prt.a = phase / 0.2f;

            col = prt;

        }

    }
}
