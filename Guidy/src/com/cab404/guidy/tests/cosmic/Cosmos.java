package com.cab404.guidy.tests.cosmic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.GameScene;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.ui.TextField;
import com.cab404.guidy.Luna;
import com.cab404.guidy.cosmic.observatory.NodeData;
import com.cab404.guidy.cosmic.ph_observatory.Bodies;
import com.cab404.guidy.cosmic.ph_observatory.Body;
import com.cab404.guidy.cosmic.ph_observatory.Line;

/**
 * @author cab404
 */
public class Cosmos extends GameScene {
    Bodies phys;

    String[] names = {
            "Luna", "Sun", "Minmus",
            "Mars", "Deimos", "Earth",
            "Hades", "Penumbra", "Gla",
            "Venus", "Node", "Gilly",
            "Pacman", "Planet", "Z",
            "Gist", "Gery", "Nort"
    };

    String name() {
        return names[rnd.nextInt(names.length)] + " " + rnd.nextInt(9999) + 1;
    }

    Body gen() {
        return new Body() {
            String name = name();
            @Override public String toString() {
                return name;
            }

            @Override public void onMouseOver(Vector2 mousePos) {
                ListMenu props = new ListMenu();
                props.animationSpeed = Float.MIN_VALUE;
                props.add(new TextField() {
                    @Override public String getText() {
                        StringBuilder b = new StringBuilder();
                        b.append("Название: ").append(name).append("\n");
                        b.append("Масса: ").append(mass).append("\n");
                        b.append("Скорость: ").append(vel.len()).append("\n");
                        return b.toString();
                    }
                    int live = 1;
                    @Override public boolean isDead() {
                        return live-- <= 0;
                    }
                });
                props.pos.set(objects.gfx.convertTo(parent.gfx, mousePos));
                objects.add(props);
                super.onMouseOver(mousePos);    //To change body of overridden methods use File | Settings | File Templates.
            }

            @Override public void update(Bodies parent) {
                super.update(parent);
                if (pos.x + pos.y > 200000000) isAlive = false;
            }
        };
    }

    Color getColor(float mass) {
        float mn = mass / 1000000;
        mn = mn > 1 ? 1 : mn;
        return Color.YELLOW.cpy().mul(1 - mn).add(Color.RED.cpy().mul(mn));
    }

    @Override public void create() {
        super.create();

        time_zoom = 0f;

        ZoomInputProcessor zip = new ZoomInputProcessor(game.gfx);
        zip.delta = 0.1f;
        Gdx.input.setInputProcessor(zip);

        phys = new Bodies();
        phys.updates_per_second = 5;
        phys.G = 1;
        game.add(phys);

        NodeData.generate();

        Body star = gen();
        phys.add(star);
        star.mass = 20000;
        star.density = 100;
        star.spr = new Sprite(NodeData.star);
        star.synchronizeSize();
        star.spr.setColor(getColor(star.mass));

        for (int i = 0; i < 800; i++) {
            Body planet = gen();

            phys.add(planet);

            planet.density = 100;
            planet.mass = 1000 + rnd.nextInt(1000);
            planet.spr = new Sprite(NodeData.star);
            planet.synchronizeSize();

            planet.spr.setColor(getColor(planet.mass));

            planet.pos.set((float) Math.pow(rnd.nextInt(30), 2) + 40, 0);
            planet.pos.rotate(rnd.nextFloat() * 360);
            planet.setOnOrbit(star, true);
        }

        Bodies.BodiesUpdateListener collistener = new Bodies.BodiesUpdateListener() {
            @Override public void update(Body b1, Body b2) {
                if (b1.pos.dst2(b2.pos) < Math.pow((b1.size.x + b2.size.x) / 2, 2)) {
                    Body alive = b1.mass > b2.mass ? b1 : b2;
                    Body dead = b1.mass > b2.mass ? b2 : b1;

                    alive.vel.mul(alive.mass / (alive.mass + dead.mass));
                    dead.vel.mul(dead.mass / (alive.mass + dead.mass));
                    alive.vel.add(dead.vel);

                    alive.mass = alive.mass + dead.mass;

                    alive.spr.setColor(getColor(alive.mass));
                    alive.synchronizeSize();

                    dead.isAlive = false;
                }
            }
        };
//        phys.remove(star);

        phys.bulisteners.add(collistener);
    }

    boolean voiding = false;
    Vector2 p1, p2;
    int mass;

    @Override public void update() {
        super.update();

        if (Gdx.input.justTouched() && Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            voiding = true;
            p1 = new Vector2(Luna.mouse(0));
            p1 = game.gfx.convertToGFX(p1);
            ListMenu menu = new ListMenu() {
                @Override public boolean isDead() {
                    return !voiding;
                }
            };
            menu.animationSpeed = Float.MIN_VALUE;
            menu.add(new TextField() {
                @Override public String getText() {
                    return mass + "";
                }
                @Override public boolean isDead() {
                    return !voiding;
                }
            });
            objects.add(menu);

        }
        if (voiding) {
            p2 = new Vector2(Luna.mouse(0));
            p2 = game.gfx.convertToGFX(p2);

            Line line = new Line(objects.gfx.convertTo(game.gfx, p1), objects.gfx.convertTo(game.gfx, p2), Color.BLUE, 2) {
                int dc = 1;
                @Override public void update(float time) {
                    super.update(time);
                    if (dc-- <= 0) isAlive = false;
                }
            };
            objects.add(line);

            if (Luna.key(Input.Keys.W)) mass += 1000;
            if (Luna.key(Input.Keys.S)) mass -= 1000;
            mass = mass < 1000 ? 1000 : mass;

            if (!Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
                voiding = false;
                Body body = gen();
                body.spr = new Sprite(NodeData.star);
                body.mass = mass;
                body.density = 100;
                body.vel = p1.cpy().sub(p2);
                body.pos.set(p1);
                body.spr.setColor(getColor(body.mass));

                body.synchronizeSize();
                phys.add(body);
            }
        }
        if (Luna.key(Input.Keys.NUM_1))
            time_zoom += 0.01f;
        if (Luna.key(Input.Keys.NUM_2))
            time_zoom -= 0.01f;
        if (Luna.key(Input.Keys.NUM_3))
            time_zoom = 1f;

        if (time_zoom < 0) time_zoom = 0;

    }

    @Override public void render() {
        super.render();
    }
}
