package com.cab404.guidy.tests.fluttershy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.GameScene;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.ui.SelectableField;
import com.cab404.defense.ui.TableMenu;
import com.cab404.defense.ui.TextField;
import com.cab404.guidy.Luna;
import com.cab404.guidy.rpg.PlayerObject;
import com.cab404.guidy.rpg.RPGGameObj;
import com.cab404.guidy.rpg.dialog.Phrase;
import com.cab404.guidy.rpg.dialog.Variant;
import com.cab404.guidy.rpg.items.Item;
import com.cab404.guidy.rpg.top_view.BoundingObject;
import com.cab404.guidy.rpg.top_view.TiledMapWrapper;
import javolution.util.FastList;

/**
 * @author cab404
 */
public class Cottage extends GameScene {
    FastList<BoundingObject> bb_storage = new FastList<>();

    TiledMapWrapper wrapper = new TiledMapWrapper();
    PlayerObject fluttershy;
    Vector2[] vectorz = new Vector2[]{
            new Vector2(554, 489),
            new Vector2(406, 319),
            new Vector2(215, 340),
            new Vector2(174, 463),
            new Vector2(553, 361),
            new Vector2(235, 214),
            new Vector2(508, 435),
            new Vector2(98, 272),
            new Vector2(41, 375),
    };
    int twiligths_remaining = vectorz.length;

    static Sprite twi_box = new Sprite(new Texture("data/imgs/box.png"));

    class TwiInTheBox extends Item {
        {
            name =
                    "Твайлайт в коробке";
            description =
                    "Испуганный клон Твайлайт Спаркл, компактно \nсложенный и помещённый в коробку.";
            cost = 1200;
            id = 1;
            parent = objects;
        }

        @Override
        public void render(SpriteBatch batch) {
            render(batch, pos, 1);
        }


        public void render(SpriteBatch batch, Vector2 lbc, float phase) {
            Vector2 pos = lbc.cpy();
            Vector2 size = getSize(phase);

            twi_box.setPosition(lbc.x, lbc.y);
            twi_box.setSize(size.x, size.y);
            twi_box.draw(batch);
            renderQuantity(batch, lbc);
        }

        @Override
        public void render(SpriteBatch batch, float phase, Vector2 lbc) {
            render(batch, lbc, phase);
        }
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void create() {
        super.create();
        wrapper.init("data/maps/flutterhouse", 16);

        game.add(wrapper);
        bb_storage.add(wrapper);

        fluttershy = new PlayerObject() {
            Sprite flutter = new Sprite(new Texture(Gdx.files.local("data/imgs/chars/fluttershy.png")));

            @Override
            public void render(SpriteBatch batch) {
                flutter.setPosition(pos.x, pos.y);
                flutter.draw(batch);
            }

            @Override
            public void interact(RPGGameObj source) {
                final TableMenu menu = items.getAsMenu(4);
                final ListMenu meh = new ListMenu() {
                    @Override
                    public boolean isDead() {
                        return fields.size() != 0;
                    }
                };
                meh.add(menu);
                meh.add(new SelectableField() {
                    @Override
                    public void onSelect(Vector2 mouse) {
                        menu.killall();
                        meh.killall();
                        time_zoom = 1;
                    }

                    @Override
                    public String getText() {
                        return "Закрыть";
                    }
                });
                time_zoom = 0;
                meh.pos.set(objects.gfx.convertTo(game.gfx, pos.cpy().add(size)));
                objects.add(meh);

            }

            @Override
            public void update(float time) {
                super.update(time);
                if (Gdx.input.justTouched())
                    if (Gdx.input.isKeyPressed(Input.Keys.C)) Luna.log(pos);

                Vector2 move = new Vector2(0, 0);
                if (Gdx.input.isKeyPressed(Input.Keys.W)) move.add(0, 2);
                if (Gdx.input.isKeyPressed(Input.Keys.S)) move.add(0, -2);
                if (Gdx.input.isKeyPressed(Input.Keys.D)) move.add(2, 0);
                if (Gdx.input.isKeyPressed(Input.Keys.A)) move.add(-2, 0);

                if (move.x > 0) flutter.flip(!flutter.isFlipX(), false);
                if (move.x < 0) flutter.flip(flutter.isFlipX(), false);

                // Погнали. Пройдёмся по interact-ам.
                if (Gdx.input.justTouched() && Gdx.input.isButtonPressed(Input.Buttons.MIDDLE)) {
                    Vector2 mouse = parent.gfx.convertToGFX(Luna.mouse(0));
                    for (BoundingObject object : bb_storage) {
                        if (object instanceof RPGGameObj)
                            if (object.getBounds().contains(mouse.x, mouse.y))
                                ((RPGGameObj) object).interact(this);
                    }
                }
                move(move);
            }
        };

        fluttershy.setBoundingObjects(bb_storage);
        fluttershy.pos = new Vector2(318, 8);
        fluttershy.size = new Vector2(16, 16);
        game.add(fluttershy);
        bb_storage.add(fluttershy);


        Luna.CameraLinkParams params = new Luna.CameraLinkParams();
        params.target = fluttershy;
        params.bounds = wrapper.getBounds();
        params.zoom = 3;
        game.add(Luna.getCameraLink(params));

        for (Vector2 pos : vectorz) {
            // Твайлайты, тысячи их!
            RPGGameObj twi = new RPGGameObj() {
                Sprite spr = new Sprite(new Texture(Gdx.files.local("data/imgs/chars/twilight.png")));

                @Override
                public void render(SpriteBatch batch) {
                    spr.setPosition(pos.x, pos.y);
                    spr.draw(batch);
                }

                @Override
                public void update(float time) {
                    if (Math.random() > 0.99) spr.flip(true, false);
                }

                @Override
                public void interact(RPGGameObj source) {
                    if (!isInRange(source, 40)) return;
                    // Ставим timelock (yay, это звучит круче, чем пауза)
                    time_zoom = 0;
                    final ListMenu menu = new ListMenu() {
                        @Override
                        public void update(float time) {
                            super.update(time);
                            if (fields.size() == 0) {
                                isAlive = false;
                                // Снимаем timelock (ахххх...)
                                time_zoom = 1;
                            }
                        }
                    };

                    Phrase ask = new Phrase
                            (
                                    "Twilight Sparkle",
                                    " - ОБОЖЕМОЙ ЭТО ЛЕВ!"
                            );

                    ask.variants.add(new Variant(" - РРРР!!!") {
                        @Override
                        public void selected() {
                            menu.killall();
                            isAlive = false;
                            fluttershy.items.put(new TwiInTheBox());

                        }
                    });

                    menu.add(ask.asMenuFields());
                    menu.pos.set(objects.gfx.convertToGFX(parent.gfx.convertFromGFX(pos.cpy().add(size))));
                    objects.add(menu);

                }
            };

            twi.pos.set(pos);
            twi.size = new Vector2(16, 16);

            bb_storage.add(twi);
            game.add(twi);

        }

        ListMenu menu = new ListMenu() {
            @Override
            public void update(float time) {
                super.update(time);
                if (fields.size() == 0) isAlive = false;
            }
        };
        menu.add(new TextField() {
            @Override
            public String getText() {
                int rem = vectorz.length;
                for (Item item : fluttershy.items.content) {
                    rem -= item.id == 1 ? item.quantity : 0;
                }

                return rem == 0 ? "Все Твайлайты собраны и упакованы!" : "Твайлайтов осталось: " + rem;
            }
        });

        objects.add(menu);

    }

}
