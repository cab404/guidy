package com.cab404.guidy.tests.circles;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.SimpleScene;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.ui.SelectableField;
import com.cab404.guidy.Luna;
import com.cab404.guidy.tests.MainMenu;

import java.util.Random;

/**
 * @author cab404
 */
public class Circles extends SimpleScene {

    @Override
    public void create() {
        super.create();
        Vector2 scr = Luna.screen();
        for (int i = 0; i != 100; i++) {
            Circle circle = new Circle(
                    new Vector2(
                            rnd.nextInt((int) scr.x * 2) - scr.x,
                            rnd.nextInt((int) scr.y * 2) - scr.y
                    ),
                    new Vector2(0, 0),
                    rnd.nextInt(60) + 20
            );
            circle.speed = rnd.nextFloat() * 100 - 50;
            circle.tint = getRandomColor().cpy();
            circle.tint.a = 0.7f;
            objects.add(circle);
        }
        for (int i = 0; i != 100; i++) {
            Circle circle = new Circle(
                    new Vector2(
                            rnd.nextInt((int) scr.x * 2) - scr.x,
                            rnd.nextInt((int) scr.y * 2) - scr.y
                    ),
                    scr.cpy(),
                    rnd.nextInt(60) + 20
            );
            circle.speed = rnd.nextFloat() * 20 - 10;
            circle.tint = getRandomColor().cpy();
            circle.tint.a = 0.7f;
            objects.add(circle);
        }

        ListMenu back = new ListMenu();
        back.add(new SelectableField() {
            @Override
            public void onSelect(Vector2 mouse) {
                Luna.scene.changeScene(new MainMenu());
            }

            @Override
            public String getText() {
                return ":: Назад;";
            }
        });

        objects.add(back);

    }

    Random rnd = new Random();

    public Color getRandomColor() {
        switch (rnd.nextInt(9)) {
            case 0:
                return Color.BLUE;
            case 1:
                return Color.CYAN;
            case 2:
                return Color.DARK_GRAY;
            case 3:
                return Color.GRAY;
            case 4:
                return Color.RED;
            case 5:
                return Color.ORANGE;
            case 6:
                return Color.PINK;
            case 7:
                return Color.GREEN;
            case 8:
                return Color.LIGHT_GRAY;
            default:
                return Color.WHITE;
        }
    }

}
