package com.cab404.guidy.tests.circles;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.guidy.cosmic.observatory.Node;
import com.cab404.guidy.cosmic.observatory.NodeData;

/**
 * @author cab404
 */
public class Circle extends GameObj {
    static Texture circle;

    static {
        int r = 250;
        Pixmap c = new Pixmap(r * 2 + 1, r * 2 + 1, Pixmap.Format.RGBA8888);
        c.setColor(Color.WHITE);
        NodeData.drawCircle(c, r, r, r - 1);
        Color smooth = Color.WHITE.cpy();
        smooth.a = 0.5f;
        c.setColor(smooth);
        NodeData.drawCircle(c, r, r, r);
        circle = new Texture(c);
    }

    public Vector2 radius;
    public Sprite cir;
    public int r;
    public float s_r;
    public float speed = 1f;
    public Color tint = Color.WHITE.cpy();

    public Circle(Vector2 radius, Vector2 pos, int size) {
        this.radius = radius;
        this.pos = pos;
        cir = new Sprite(circle);
        r = size;
        s_r = radius.len();
        cir.setSize(r * 2, r * 2);
    }

    @Override
    public void render(SpriteBatch batch) {
        cir.setColor(tint);
        Vector2 rpos = pos.cpy().add(radius);
        cir.setPosition(rpos.x - r, rpos.y - r);
        cir.draw(batch);
    }

    @Override
    public void update(float time) {
        radius.rotate(speed / (s_r * Node.pi) * 360 * time);
    }
}
