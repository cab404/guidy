package com.cab404.guidy.rpg.top_view;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализует воксельную физику, и то не до конца.
 * Для рогалика с видом сверху или платформера очень даже.
 *
 * @author cab404
 */
public abstract class BoundingObject extends GameObj {

    /**
     * Статичный ли объект.
     */
    public boolean _static = true;
    /**
     * Список объектов для проверки столкновений.
     */
    private List<BoundingObject> boundingObjects;


    @Override
    public abstract void render(SpriteBatch batch);

    @Override
    public void update(float time) {
        ArrayList<BoundingObject> toDelete = new ArrayList<>();
        for (BoundingObject object : boundingObjects)
            if (!object.isAlive) {
                toDelete.add(object);
            }
        boundingObjects.removeAll(toDelete);
    }

    /**
     * Двигает тело с обработкой столкновений в заданном направлении.
     */
    public void move(Vector2 there_to) {
        float pr = 0.1f;
        Vector2 v = there_to.cpy();
        while (v.x != 0 || v.y != 0) {

            if (Math.abs(v.x) > 0)
                if (Math.abs(v.x) < pr) {
                    if ((v.x > 0 ? moveRight(v.x) : moveLeft(v.x)) < pr) v.x = 0;
                    v.x = 0;
                } else {
                    if ((v.x > 0 ? moveRight(pr) : moveLeft(pr)) < pr) v.x = 0;
                    else
                        v.x -= pr * Math.signum(v.x);
                }

            if (Math.abs(v.y) > 0)
                if (Math.abs(v.y) < pr) {
                    if ((v.y > 0 ? moveUp(v.y) : moveDown(v.y)) < pr) v.y = 0;
                    v.y = 0;
                } else {
                    if ((v.y > 0 ? moveUp(pr) : moveDown(pr)) < pr) v.y = 0;
                    else
                        v.y -= pr * Math.signum(v.y);
                }

        }
    }

    public float moveUp(float move_by) {
        // Проходимся по длине тела
        float moved = move_by;
        for (BoundingObject object : boundingObjects) {
            if (object == this) continue;
            BoundingObject collide = null;
            Vector2 dot = null;

            for (int i = 0; i <= size.x; i++) {
                dot = new Vector2(pos.x + (i == 0 ? 0 : (float) i - 0.001f), pos.y + size.y + move_by);
                if (object.isSolid(dot)) {
                    collide = object;
                    break;
                }

            }
            if (collide != null) {
                onHit(Side.TOP, collide);
                if (!object._static) {
                    moved = Math.min(moved, object.moveUp(moved));
                } else {
                    float obj_pos_y = object.getSideCoordinates(dot, Side.BOTTOM);
                    moved = obj_pos_y - (pos.y + size.y);
                }
            }
        }
        pos.add(0, moved);
        return moved;
    }

    public float moveDown(float move_by) {
        // Проходимся по длине тела
        float moved = move_by;
        for (BoundingObject object : boundingObjects) {
            if (object == this) continue;
            BoundingObject collide = null;
            Vector2 dot = null;

            for (int i = 0; i <= size.x; i++) {
                dot = new Vector2(pos.x + (i == 0 ? 0 : (float) i - 0.001f), pos.y - move_by);
                if (object.isSolid(dot)) {
                    collide = object;
                    break;
                }

            }
            if (collide != null) {
                onHit(Side.BOTTOM, collide);
                if (!object._static) {
                    moved = Math.min(moved, object.moveDown(moved));
                } else {
                    float obj_pos_y = object.getSideCoordinates(dot, Side.TOP);
                    moved = pos.y - obj_pos_y;
                }
            }
        }
        pos.add(0, -moved);
        return moved;
    }

    public float moveLeft(float move_by) {
        // Проходимся по длине тела
        float moved = move_by;
        for (BoundingObject object : boundingObjects) {
            if (object == this) continue;
            BoundingObject collide = null;
            Vector2 dot = null;

            for (int i = 0; i <= size.y; i++) {
                dot = new Vector2(pos.x - move_by, pos.y + (i == 0 ? 0 : (float) i - 0.001f));
                if (object.isSolid(dot)) {
                    collide = object;
                    break;
                }

            }
            if (collide != null) {
                onHit(Side.LEFT, collide);
                if (!object._static) {
                    moved = Math.min(moved, object.moveLeft(moved));
                } else {
                    float obj_pos_x = object.getSideCoordinates(dot, Side.RIGHT);
                    moved = pos.x - obj_pos_x;
                }
            }
        }
        pos.add(-moved, 0);
        return moved;
    }

    public float moveRight(float move_by) {
        // Проходимся по длине тела
        float moved = move_by;
        for (BoundingObject object : boundingObjects) {
            if (object == this) continue;
            BoundingObject collide = null;
            Vector2 dot = null;

            for (int i = 0; i <= size.y; i++) {
                dot = new Vector2(pos.x + size.x + move_by, pos.y + (i == 0 ? 0 : (float) i - 0.001f));
                if (object.isSolid(dot)) {
                    collide = object;
                    break;
                }

            }
            if (collide != null) {
                onHit(Side.RIGHT, collide);
                if (!object._static) {
                    moved = Math.min(moved, object.moveRight(moved));
                } else {
                    float obj_pos_x = object.getSideCoordinates(dot, Side.LEFT);
                    moved = obj_pos_x - (pos.x + size.x);
                }
            }
        }
        pos.add(moved, 0);
        return moved;
    }

    public void setBoundingObjects(List<BoundingObject> boundingObjects) {
        this.boundingObjects = boundingObjects;
    }

    public List<BoundingObject> getBoundingObjects() {
        return boundingObjects;
    }


    public enum Side {
        LEFT, RIGHT, TOP, BOTTOM
    }

    public void onHit(Side side, BoundingObject object) {}

    public float getSideCoordinates(Vector2 dot, Side side) {
        switch (side) {
            case BOTTOM:
                return pos.y;
            case TOP:
                return pos.y + size.y;
            case LEFT:
                return pos.x;
            case RIGHT:
                return pos.x + size.x;
            default:
                throw new Error("WAT?!");
        }
    }

    public boolean isSolid(Vector2 position) {
        return getBounds().contains(position.x, position.y);
    }

}
