package com.cab404.guidy.rpg.top_view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.tiled.SimpleTileAtlas;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLayer;
import com.badlogic.gdx.graphics.g2d.tiled.TiledLoader;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;

/**
 * Обёртка для тайловой карты.
 *
 * @author cab404
 */
public class TiledMapWrapper extends BoundingObject {

    public SimpleTileAtlas atlas;
    public TiledMap map;
    private boolean[][] atlas_map;
    private int tile_size;
    public boolean isSolidOutside = true;


    /**
     * Загружает карту по параметрам. Предполагается, что тайлы квадратные.
     *
     * @param path_to_map Путь к папке с картой map.tmx и атласом tiles.png, и возможно, collision map-ом tiles.cm.
     * @param tile_side   Размер тайла на экране.
     */
    public void init(String path_to_map, int tile_side) {
        this.tile_size = tile_side;
        FileHandle map_folder = Gdx.files.internal(path_to_map);

        map = TiledLoader.createMap(map_folder.child("map.tmx"));
        atlas = new SimpleTileAtlas(map, map_folder);

        if (map_folder.child("tiles.cm").exists())
            atlas_map = PU.readFromStream(map_folder.child("tiles.cm").read());
        else
            atlas_map = PU.generateCollisionMap(map_folder.child("tiles.png").read());

        this.size = new Vector2(tile_side * map.width, tile_side * map.height);

    }

    public TiledMapWrapper() {}

    public TiledMapWrapper(String path_to_map, int texture_size) {
        init(path_to_map, texture_size);
    }

    @Override
    public void onMouseOver(Vector2 mousePos) {
    }

    @Override
    /**
     * Возвращает, прозрачен ли последний слой в данной области
     */
    public boolean isSolid(Vector2 pos) {
        // Глобальные координаты
        float mul = ((float) map.tileWidth / (float) tile_size);
        int x = (int) Math.floor(pos.x - this.pos.x);
        int y = (int) Math.floor(pos.y - this.pos.y);

        if (x < this.pos.x) return isSolidOutside;
        if (y < this.pos.y) return isSolidOutside;
        if (x >= this.pos.x + this.size.x) return isSolidOutside;
        if (y >= this.pos.y + this.size.y) return isSolidOutside;

        // Координаты тайла
        int gx = (int) Math.floor((float) x / tile_size);
        int gy = (int) Math.floor((float) y / tile_size);

        // Координаты пикселя на тайле
        int tx = (int) ((x - gx * tile_size) * mul);
        int ty = (int) ((y - gy * tile_size) * mul);

        TiledLayer layer = map.layers.get(1);
        TileInfo info = new TileInfo(layer.tiles[map.height - gy - 1][gx]);

        if (info.tileID == 0) return false;

        if (info.flipX) tx = map.tileWidth - tx - 1;
        if (!info.flipY ^ info.flipD) ty = map.tileHeight - ty - 1;

        if (info.flipD) {
            if (info.flipX ^ info.flipY) {
                int tmp = ty;
                ty = tx;
                tx = map.tileWidth - tmp - 1;
            } else {
                int tmp = tx;
                tx = ty;
                ty = map.tileHeight - tmp - 1;
            }
        }

        TextureRegion tile = atlas.getRegion(info.tileID);
        return atlas_map[tile.getRegionX() + tx][tile.getRegionY() + ty];
    }

    @Override
    public void render(SpriteBatch batch) {
        int sx, sy, ex, ey;

        // Погнали. Вычисляем, что рисуется, а что - нет.

        float cam_zoom = this.parent.gfx.cam.zoom;
        float cam_width = this.parent.gfx.cam.viewportWidth * cam_zoom;
        float cam_height = this.parent.gfx.cam.viewportHeight * cam_zoom;

        Vector2 cam_pos = this.parent.gfx.cam_pos();
        Vector2 delta = cam_pos.sub(this.pos);

        // Ищем левый верхний угол.

        sx = (int) Math.ceil(delta.x / tile_size) - 2;
        sy = (int) Math.ceil(delta.y / tile_size) - 2;

        // И правый нижний.

        ex = (int) Math.floor(cam_width / tile_size) + sx + 3;
        ey = (int) Math.floor(cam_height / tile_size) + sy + 3;

        // Проверяем на AIOoB

        sx = sx < 0 ? 0 : sx;
        sy = sy < 0 ? 0 : sy;
        ex = ex < 0 ? 0 : ex;
        ey = ey < 0 ? 0 : ey;

        sx = sx > map.width ? map.width : sx;
        sy = sy > map.height ? map.height : sy;
        ex = ex > map.width ? map.width : ex;
        ey = ey > map.height ? map.height : ey;

        // Ну и рисуем.

        for (TiledLayer layer : map.layers) {
            for (int x = sx; x < map.width && x < ex; x++) {
                for (int y = map.height - ey; y < map.height - sy; y++) {

                    int id = layer.tiles[y][x];
                    TileInfo info = new TileInfo(id);

                    Vector2 pos = this.pos.cpy();
                    pos.add(tile_size * x, tile_size * (map.height - y - 1));

                    TextureRegion tile;

                    if ((tile = atlas.getRegion(info.tileID)) != null) {
                        Sprite spr = new Sprite(tile);
                        spr.setScale(1.001f);
                        spr.setPosition(pos.x, pos.y);
                        spr.setSize(tile_size, tile_size);

                        spr.flip(info.flipX, info.flipD ^ info.flipY);
                        if (info.flipD) spr.rotate90(!(info.flipX ^ info.flipY));

                        spr.draw(batch);
                    }

                }

            }

        }

    }

    @Override
    public float getSideCoordinates(Vector2 dot, Side side) {
        Vector2 local = dot.sub(pos);

        switch (side) {
            case LEFT:
                return (int) Math.floor(local.x) + pos.x;
            case BOTTOM:
                return (int) Math.floor(local.y) + pos.y;

            case RIGHT:
                return (int) Math.ceil(local.x) + pos.x;
            case TOP:
                return (int) Math.ceil(local.y) + pos.y;

            default:
                throw new Error("WAT?!?");
        }
    }

    @Override
    public void update(float time) {
    }

    public class TileInfo {
        public boolean flipX, flipY, flipD;
        public int tileID;

        public TileInfo(int id) {
            tileID = id & Integer.MAX_VALUE >> 3;
            // Первые три бита
            int rotBit = (id & (7 << 29)) >> 29;
            flipX = ((rotBit & 4) == 4);
            flipY = ((rotBit & 2) == 2);
            flipD = ((rotBit & 1) == 1);
        }

    }
}
