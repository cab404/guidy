package com.cab404.guidy.rpg.top_view;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Physical Utils.
 * Да, так просто. Работает с изображениями, достаёт collision map-ы.
 *
 * @author cab404
 */
public class PU {

    public static boolean[][] generateCollisionMap(InputStream image) {
        return generateCollisionMap(image, 0, 0, -1, -1);
    }

    public static boolean[][] generateCollisionMap(InputStream image, int x, int y, int w, int h) {

        try {
            BufferedImage _atlas = ImageIO.read(image);

            if (w == -1) w = _atlas.getWidth();
            if (h == -1) h = _atlas.getHeight();

            boolean[][] atlas_map = new boolean[w][h];
            Raster data = _atlas.getData();

            for (int ax = 0; ax != w; ax++)
                for (int ay = 0; ay != h; ay++) {
                    atlas_map[ax][ay] = data.getSample(x + ax, y + ay, 3) > 0;
                }

            return atlas_map;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean[][] generateCollisionMap(Pixmap image, int x, int y, int w, int h) {
        if (w == -1) w = image.getWidth();
        if (h == -1) h = image.getHeight();

        boolean[][] atlas_map = new boolean[w][h];

        for (int ax = 0; ax != w; ax++)
            for (int ay = 0; ay != h; ay++) {
                atlas_map[ax][ay] = image.getPixel(x + ax, y + ay) << 24 > 0;
            }

        return atlas_map;
    }

    public static Pixmap generatePixmap(InputStream image, int x, int y, int w, int h) {
        try {
            BufferedImage _atlas = ImageIO.read(image);
            Pixmap px = new Pixmap(_atlas.getWidth(), _atlas.getHeight(), Pixmap.Format.RGBA8888);

            if (w == -1) w = _atlas.getWidth();
            if (h == -1) h = _atlas.getHeight();

            boolean[][] atlas_map = new boolean[w][h];
            Raster data = _atlas.getData();

            for (int ax = 0; ax != w; ax++)
                for (int ay = 0; ay != h; ay++) {
                    px.setColor(
                            data.getSample(x + ax, y + ay, 0),
                            data.getSample(x + ax, y + ay, 1),
                            data.getSample(x + ax, y + ay, 2),
                            data.getSample(x + ax, y + ay, 3)
                    );
                }

            return px;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }


    public static boolean[][] generateCollisionMap(Texture image) {
        int x, y, w, h;
        x = 0;
        y = 0;
        w = image.getWidth();
        h = image.getHeight();
        return generateCollisionMap(generatePixmap(image, x, y, w, h), x, y, w, h);
    }

    public static Pixmap generatePixmap(Texture image, int x, int y, int w, int h) {
        Pixmap px = new Pixmap(image.getWidth(), image.getHeight(), Pixmap.Format.RGBA8888);
        image.draw(px, 0, 0);
        return px;
    }


    /**
     * На практике оказалось, что генерация collision map-ов для огромных карт - жутко долгое занятие.
     */
    public static void writeToStream(boolean[][] map, OutputStream str) {
        try {
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(str)));
            StringBuilder all = new StringBuilder();

            all.append(map.length).append("\n");
            for (int y = 0; y < map.length; y++) {
                for (int x = 0; x < map[y].length; x++) all.append(map[y][x] ? "s" : "b");
                all.append("\n");
            }

            out.write(all.toString());
            out.flush();
            out.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean[][] readFromStream(InputStream input) {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new GZIPInputStream(input)));

            int height = Integer.parseInt(in.readLine());
            boolean[][] map = new boolean[height][];

            for (int y = 0; y < height; y++) {
                String row = in.readLine();
                map[y] = new boolean[row.length()];

                for (int x = 0; x < row.length(); x++)
                    map[y][x] = row.charAt(x) == 's';

            }

            in.close();

            return map;

        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    /**
     * Генератор collision map-а
     */
    public static void main(String[] args) {
        try {
            File f = new File("tiles.cm");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            boolean[][] map = generateCollisionMap(new FileInputStream(args[0]));

            writeToStream(map, fo);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
