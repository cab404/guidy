package com.cab404.guidy.rpg.dialog;

/**
* @author cab404
*/
public abstract class Variant {
    String label;

    public Variant(String label) {
        this.label = label;
    }

    public abstract void selected();
}
