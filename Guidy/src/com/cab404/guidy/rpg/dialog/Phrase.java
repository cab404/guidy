package com.cab404.guidy.rpg.dialog;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.ui.DelimeterField;
import com.cab404.defense.ui.Field;
import com.cab404.defense.ui.SelectableField;
import com.cab404.defense.ui.TextField;
import javolution.util.FastList;

/**
 * @author cab404
 */
public class Phrase {

    public String author, text;
    public FastList<Variant> variants;

    public Phrase(String author, String text) {
        this.author = author;
        this.text = text;
        variants = new FastList<>();
    }


    public Field[] asMenuFields() {
        final Field[] dia = new Field[3 + variants.size()];

        dia[0] = new TextField() {
            {
                color = Color.DARK_GRAY.cpy();
                font_color = Color.ORANGE.cpy();
            }

            @Override
            public String getText() {
                return author;
            }
        };
        dia[1] = new TextField() {
            {
                color = Color.DARK_GRAY.cpy();
                font_color = Color.GRAY.cpy();
            }
            @Override
            public String getText() {
                return text;
            }
        };
        dia[2] = new DelimeterField(new Vector2(0, 5));

        for (int i = 3; i < dia.length; i++) {
            final Variant var = variants.get(i - 3);
            dia[i] = new SelectableField() {
                @Override
                public void onSelect(Vector2 mouse) {
                    var.selected();
                }

                @Override
                public String getText() {
                    return var.label;
                }
            };
        }

        return dia;
    }
}
