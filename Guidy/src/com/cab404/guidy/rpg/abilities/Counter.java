package com.cab404.guidy.rpg.abilities;

/**
 * Эффект, длящийся несколько ходов.
 *
 * @author cab404
 */
public interface Counter extends Skill {
    /**
     * @return Закончилось ли действие эффекта?
     */
    public boolean expired();
}
