package com.cab404.guidy.rpg.abilities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cab404.defense.objects.GameObj;
import com.cab404.guidy.rpg.PlayerObject;

/**
 * Заклинание или удар копытом - этот класс.
 *
 * @author cab404
 */
public interface Skill {

    public void render(SpriteBatch batch, float phase, PlayerObject obj, boolean isTarget);

    public void apply(PlayerObject caster);

    public void getDescription(GameObj caster);

}
