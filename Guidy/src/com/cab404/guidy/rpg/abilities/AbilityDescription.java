package com.cab404.guidy.rpg.abilities;

/**
 * @author cab404
 */
public class AbilityDescription {
    public String name, description;
    public int mana_cost;
}
