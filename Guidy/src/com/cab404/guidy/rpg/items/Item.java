package com.cab404.guidy.rpg.items;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.ui.DelimeterField;
import com.cab404.defense.ui.Field;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.ui.TextField;
import com.cab404.guidy.Luna;
import com.cab404.guidy.rpg.PlayerObject;
import com.cab404.guidy.rpg.RPGGameObj;

/**
 * Просто item. Всё остальное в наследники.
 *
 * @author cab404
 */
public abstract class Item extends RPGGameObj implements Field {
    public String name, description;
    public int quantity = 1, cost, id;

    {
        _static = false;
    }

    public static Item getBlankItem() {
        return new Item() {
            {
                description = "";
                name = "";
                quantity = 0;
                cost = 0;
                id = 0;
            }

            @Override
            public void render(SpriteBatch batch, float phase, Vector2 lbc) {
            }
        };
    }


    public void renderQuantity(SpriteBatch batch, Vector2 lbc) {
        if (quantity > 0) {
            BitmapFont.TextBounds bounds = Luna.font().getBounds(quantity + "");
            Vector2 pos = lbc.cpy().add(getSize(1).x, 0).sub(bounds.width, 0).add(-1, 1);
            Luna.font().draw(batch, quantity + "", pos.x, pos.y);
        }
    }

    public Field[] getDescription() {
        Field[] fields = new Field[4];
        fields[0] = (new TextField() {
            @Override
            public String getText() {
                return name;
            }
        });
        fields[1] = (new TextField() {
            @Override
            public String getText() {
                return cost + " bit.";
            }
        });
        fields[2] = (new DelimeterField(10, 5));
        fields[3] = (new TextField() {
            @Override
            public String getText() {
                return description;
            }
        });
        return fields;
    }

    @Override
    public void interact(RPGGameObj source) {
        if (source instanceof PlayerObject) {
            if (((PlayerObject) source).items.put(this))
                isAlive = false;
        }
    }

    @Override
    public Vector2 getSize(float phase) {
        return new Vector2(32, 32).mul(phase);
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public void onMouseOver(Vector2 mousePos) {
        if (id != 0) {
            ListMenu desc = new ListMenu() {
                int toDie = 0;

                @Override
                public void update(float time) {
                    super.update(time);
                    isAlive = toDie-- > 0;
                }

            };
            desc.add(1, getDescription());
            desc.pos = mousePos;
            parent.add(desc);
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        render(batch, 1, pos);
    }

    @Override
    public void update(float time) {

    }

}
