package com.cab404.guidy.rpg.items;

import com.cab404.defense.ui.TableMenu;

/**
 * @author cab404
 */
public class Inventory {
    public Item[] content;

    public Inventory(int slots) {
        content = new Item[slots];
        for (int i = 0; i < slots; i++)
            content[i] = Item.getBlankItem();
    }

    public TableMenu getAsMenu(int width) {
        TableMenu menu = new TableMenu(width);
        menu.add(content);
        return menu;
    }

    public boolean put(Item to_put) {
        for (int i = 0; i < content.length; i++) {
            if (content[i].id == 0) {
                content[i] = to_put;
                return true;
            }
            if (content[i].id == to_put.id) {
                content[i].quantity += to_put.quantity;
                return true;
            }
        }
        return false;
    }


}
