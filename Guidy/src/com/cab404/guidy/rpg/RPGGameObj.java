package com.cab404.guidy.rpg;

import com.cab404.guidy.rpg.top_view.BoundingObject;

/**
 * @author cab404
 */
public abstract class RPGGameObj extends BoundingObject {
    public abstract void interact(RPGGameObj source);
}
