package com.cab404.guidy.rpg;

import com.cab404.guidy.rpg.abilities.Counter;
import com.cab404.guidy.rpg.abilities.Skill;
import com.cab404.guidy.rpg.items.Inventory;
import javolution.util.FastList;

/**
 * Существо, которое может сражаться и одновременно быть материальным.
 *
 * @author cab404
 */
public abstract class PlayerObject extends RPGGameObj {

    public static long getExpForLevel(long level) {
        return (long) Math.pow(2, level - 1);
    }

    public static long getLevelForExp(long exp) {
        // Да, я такой вот ленивый круп.
        return Long.toBinaryString(exp).length();
    }

    public long
            hp, maxhp,
            mp, maxmp,
            atk, luck,
            exp;

    public FastList<Counter> effects;
    public FastList<Skill> skills;
    public Inventory items;

    protected PlayerObject() {
        effects = new FastList<>();
        skills = new FastList<>();
        items = new Inventory(16);
    }
}
