package com.cab404.guidy.cosmic.ph_observatory;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.guidy.cosmic.observatory.Node;

/**
 * @author cab404
 */
public class Body extends GameObj {

    // Единица массы - заяц.
    // Единица расстояния - пиксель.
    // Единица времени - секунда.

    public float mass = 10;
    public float density = 5;
    public boolean calculatePhysics = true;
    public Sprite spr;
    public Vector2 vel;
    protected Bodies bodies;

    public Body() {
        super();
        vel = new Vector2();
    }

    public float getCurrG(Body rel) {
        return bodies.G * rel.mass /
                pos.dst2(rel.pos);
    }

    public float getCurrentEscapeVelocity(Body rel) {
        return (float) Math.sqrt(2) * getCurrentOrbitalVelocity(rel);
    }

    public float getCurrentOrbitalVelocity(Body rel) {
        return (float) Math.sqrt(getCurrG(rel) * getHeightAbove(rel));
    }

    public float getHeightAbove(Body rel) {
        return rel.pos.dst(pos);
    }

    public void applyForceToSelf(Body sec_body, float time) {
        if (calculatePhysics && sec_body.calculatePhysics) {
            Vector2 dir = sec_body.pos.cpy().sub(pos).nor();
            float g = getCurrG(sec_body);
            if (Float.isNaN(vel.x)) vel.x = 0;
            if (Float.isNaN(vel.y)) vel.y = 0;
            vel.add(dir.mul(g * time));
        }
    }

    @Override
    public void render(SpriteBatch batch) {
        spr.setPosition(pos.x - spr.getWidth() / 2, pos.y - spr.getHeight() / 2);
        spr.draw(batch);
    }

    public void synchronizeSize() {
        float r = (float) Math.cbrt(mass / density / (3f / 4f * Node.pi));
        spr.setSize(r * 2, r * 2);
    }

    @Override
    public void update(float time) {
        size.set(spr.getWidth(), spr.getHeight());
        pos.add(vel.cpy().mul(time));
    }

    public float getFullVelocity() {
        return vel.len();
    }

    public float getRelativeVelocity(Body rel) {
        return rel.vel.cpy().sub(vel).len();
    }

    public Body clone() {
        Body clone = new Body();
        clone.mass = mass;
        clone.size = size;
        clone.pos = pos.cpy();
        clone.vel = vel.cpy();
        clone.spr = spr;
        clone.parent = parent;
        return clone;
    }

    public Body getOrbiting(Bodies there_to_search) {
        Body out = new Body();
        float currentG = 0;
        for (Body body : there_to_search.bodies) {
            if (body == this) continue;
            float temp = getCurrG(body);
            if (currentG < temp) {
                currentG = temp;
                out = body;
            }
        }
        return out;
    }

    public void setOnOrbit(Body rel, boolean clockwise) {
        vel.set(rel.vel);
        Vector2 dir = rel.pos.cpy().sub(pos).nor();
        vel
                .add(dir.rotate(clockwise ? 90 : -90)
                        .mul(getCurrentOrbitalVelocity(rel)));
    }

    @Override public Rectangle getBounds() {
        Rectangle bounds = super.getBounds();
        bounds.x -= size.x / 2;
        bounds.y -= size.y / 2;
        return bounds;
    }
    public void update(Bodies parent) {

    }

}
