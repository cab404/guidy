package com.cab404.guidy.cosmic.ph_observatory;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import javolution.util.FastList;

import java.util.ArrayList;
import java.util.Collections;

/**
 * @author cab404
 */
public class Bodies extends GameObj {
    public float G = 6.672f;
    public int updates_per_second = 10;
    public FastList<Body> bodies;
    public ArrayList<BodiesUpdateListener> bulisteners;

    public interface BodiesUpdateListener {
        public void update(Body b1, Body b2);
    }

    public Bodies() {
        bodies = new FastList<>();
        bulisteners = new ArrayList<>();
    }

    public Bodies clone() {
        Bodies clone = new Bodies();
        for (Body body : bodies) {
            clone.bodies.add(body.clone());
        }
        return clone;
    }

    public void update(float time) {
        for (int ignored = 0; ignored < updates_per_second; ignored++)

            for (int i = 0; i < bodies.size(); i++) {
                Body current = bodies.get(i);

                if (!current.isAlive) bodies.remove(current);
                current.parent = parent;

                if (current.calculatePhysics)
                    for (int j = i + 1; j < bodies.size(); j++) {
                        Body interfering = bodies.get(j);

                        for (BodiesUpdateListener ls : bulisteners) ls.update(current, interfering);

                        if (interfering.calculatePhysics) {
                            current.applyForceToSelf(interfering, time / updates_per_second);
                            interfering.applyForceToSelf(current, time / updates_per_second);
                        }
                    }

                current.update(time);
                current.update(this);
            }
    }

    public void render(SpriteBatch batch) {
        for (Body current : bodies) {
            current.render(batch);
        }
    }

    public void add(Body... bodies) {
        for (Body body : bodies) {
            body.parent = parent;
            body.bodies = this;
        }
        Collections.addAll(this.bodies, bodies);
    }

    @Override public void onMouseOver(Vector2 mousePos) {
        for (Body body : bodies) {
            if (body.getBounds().contains(mousePos.x, mousePos.y)) {
                body.onMouseOver(mousePos);
            }
        }
    }
    @Override public Rectangle getBounds() {
        return new Rectangle(-Float.MAX_VALUE / 2, -Float.MAX_VALUE / 2, Float.MAX_VALUE, Float.MAX_VALUE);
    }
    public void remove(Body body) {
        bodies.remove(body);
    }

}
