package com.cab404.guidy.cosmic.ph_observatory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.cosmic.observatory.NodeData;

/**
 * @author cab404
 */
public class Bullet extends Body {
    int live = 3000;

    public Bullet(Vector2 dir) {
        super();
        vel.sub(dir.cpy().nor().mul(300000));
        this.spr = new Sprite(NodeData.planet);
        this.spr.setSize(2, 2);
        mass = 5;
        calculatePhysics = false;
    }

    public Bullet(Vector2 dir, Body ref) {
        this(dir);
        this.vel.add(ref.vel);
        this.pos = ref.pos.cpy();
        this.pos.sub(dir.nor().mul((ref.size.x * 1.5f + this.size.x) / 2));
    }

    @Override
    public void update(Bodies parent) {
        for (Body body : parent.bodies) {
            if (this != body && body.pos.dst2(pos) <= Math.pow((this.size.x + body.size.x) / 2, 2)) {
                this.isAlive = false;
                body.synchronizeSize();
                body.mass -= 1;
                if (body.mass <= 0) {
                    for (int i = 0; i != 10; i++)
                        parent.add(new Particle(body, 600, Math.random() > 0.7 ? Color.GRAY.cpy() : Color.RED.cpy()));
                    body.isAlive = false;
                }
                break;
            }
        }
        live--;
        if (live <= 0) isAlive = false;
    }


}
