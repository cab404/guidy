package com.cab404.guidy.cosmic.ph_observatory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;

/**
 * @author cab404
 */
public class Line extends GameObj{

    static Texture pixel;
    static {
        Pixmap pix = new Pixmap(1,1, Pixmap.Format.RGBA8888);
        pix.setColor(Color.WHITE);
        pix.drawPixel(0,0);
        pixel = new Texture(pix);
    }

    Sprite line;
    public Line(Vector2 start, Vector2 destination, Color color, float width){
        super();
        Vector2 lineVector = start.cpy().sub(destination);
        Vector2 position = start.cpy();//.add(lineVector.cpy().div(2));
        pos.set(position);

        float rot = lineVector.angle() + 90;

        line = new Sprite(pixel);
        line.setSize(width, lineVector.len());
        line.setColor(color);
        line.setRotation(rot);
        line.setPosition(pos.x, pos.y);
    }

    @Override
    public void render(SpriteBatch batch) {
        line.draw(batch);
        isAlive = false;
    }

    @Override
    public void update(float time) {
    }
}
