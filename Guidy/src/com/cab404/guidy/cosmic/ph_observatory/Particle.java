package com.cab404.guidy.cosmic.ph_observatory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * @author cab404
 */
public class Particle extends Body {

    static {
        Pixmap tex = new Pixmap(100, 100, Pixmap.Format.RGBA8888);
        tex.setColor(Color.WHITE);
        for (int x = 0; x != 100; x++)
            for (int y = 0; y != 100; y++)
                if (new Vector2(x, y).len2() < 100)
                    if (Math.random() > 0.7)
                        tex.drawPixel(x, y);
        particle = new Texture(tex);
    }

    static Texture particle;

    int delay = 600;
    int sDelay = 600;

    public Particle(Body emmiter, int delay, Color col) {
        super();
        calculatePhysics = false;
        this.sDelay = delay;
        this.delay = delay;
        this.pos.set(pos);
        vel.set(emmiter.vel);
        vel.add(
                (float) Math.pow((Math.random() - 0.5) * 200, 2),
                (float) Math.pow((Math.random() - 0.5) * 200, 2)
        );
        spr = new Sprite(particle);
        spr.setSize(5,5);
        spr.setColor(col);
    }

    @Override
    public void update(float time) {
        spr.setPosition(pos.x, pos.y);
        spr.getColor().a = delay / sDelay;
        pos.add(vel.cpy().mul(time));
        isAlive = delay > 0;
        delay--;
        if (delay <= 0) isAlive = false;
    }

}
