package com.cab404.guidy.cosmic.observatory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import javolution.util.FastList;

/**
 * @author cab404
 */
public class Node extends GameObj {
    private Node parent;
    private FastList<Node> children;
    private float height, velosity;
    private Vector2 relativePos;
    private float mass;
    private float density;
    public Sprite spr;
    public static final float pi = 3.141f, G = 6.672f;
    ;

    public Node() {
        children = new FastList<>();
        velosity = 0;
        mass = 20000;
        height = 0;
        density = 1f;
        spr = new Sprite(NodeData.star);
        spr.setColor(Color.ORANGE);
        configureSprite();
    }

    public Node(Node parent) {
        this();
        mass = 10;
        density = 10;
        height = 100;

        spr = new Sprite(NodeData.planet);
        spr.setColor(Color.BLUE);

        this.parent = parent;
        relativePos = new Vector2(0, height);

        velosity = calculateAngularVelosity();

        parent.children.add(this);
        configureSprite();
    }


    public float calculateAngularVelosity() {
        float m = parent.mass;
        float p = height * pi;
        return (float) (p / Math.sqrt(G * m / height)) * 360f;

    }

    public void configureSprite() {
        float volume = mass / density;
        float r = (float) Math.cbrt(volume / (4f / 3f * pi));
        spr.setSize(r * 2, r * 2);
    }

    public void draw(SpriteBatch batch, float x, float y) {
        spr.setPosition(x - spr.getWidth() / 2, y - spr.getHeight() / 2);
        spr.draw(batch);
        Vector2 pos = new Vector2(x, y);
        for (Node child : children) {
            Vector2 child_pos = pos.cpy().add(child.relativePos);
            child.draw(batch, child_pos.x, child_pos.y);
        }

    }

    @Override
    public void render(SpriteBatch batch) {
        draw(batch, 0, 0);
    }

    public Node setHeight(float height) {
        this.height = height;
        if (parent == null) return this;
        velosity = calculateAngularVelosity();
        relativePos.nor().mul(height);
        return this;
    }

    public Node setMass(float mass) {
        this.mass = mass;
        configureSprite();
        if (parent == null) return this;
        velosity = calculateAngularVelosity();
        return this;
    }

    public Node setDensity(float density) {
        this.density = density;
        configureSprite();
        return this;
    }

    public void update(float time) {
        for (Node child : children) {
            child.relativePos.rotate(child.velosity * time);
            child.update(time);
        }
    }
}
