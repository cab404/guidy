package com.cab404.guidy.cosmic.observatory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;

/**
 * Рисует звезду и планету. 96
 *
 * @author cab404
 */
public class NodeData {
    public static Texture star, planet;

    public static void generate() {
        int r = 300;
        Pixmap round = new Pixmap(r * 2 + 1, r * 2 + 1, Pixmap.Format.RGBA8888);
        for (int i = 0; i != r; i++) {
            float col = (1 - (float) i / r);
            col = (float) Math.sqrt(col) / 10;
            round.setColor(1, 1, 1, col);
            drawCircle(round, r, r, i);
        }
        star = new Texture(round);

        Pixmap round2 = new Pixmap(r * 2 + 1, r * 2 + 1, Pixmap.Format.RGBA8888);
        round2.setColor(Color.WHITE);
        drawCircle(round2, r, r, r);

        planet = new Texture(round2);

    }

    public static void drawCircle(Pixmap pixmap, int x, int y, int r) {
        int sr = r * r;
        for (int px = x - r; px != x + r; px++) {
            for (int py = y - r; py != x + r; py++) {
                if ((px - x) * (px - x) + (py - y) * (py - y) < sr) pixmap.drawPixel(px, py);
            }
        }
    }
}
