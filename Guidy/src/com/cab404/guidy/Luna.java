package com.cab404.guidy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.defense.scene.SceneManager;
import com.cab404.defense.storage.SpriteStorage;
import com.cab404.defense.util.FL;
import com.cab404.defense.util.SL;

import java.util.HashMap;

/**
 * Ave Luna!
 * Различные статичные методы и переменные, по типу Gdx
 *
 * @author cab404
 */
public class Luna {

    public static SceneManager scene;
    public static SpriteStorage sprites;

    /**
     * Shortcut для Gdx.input.isKeyPressed
     */
    public static boolean key(int id) {
        return Gdx.input.isKeyPressed(id);
    }

    public static BitmapFont font() {
        return FL.getInstance().def();
    }

    public static BitmapFont font(int id) {
        return FL.getInstance().font(id);
    }

    /**
     * Возвращает рамер экрана
     */
    public static Vector2 screen() {
        return new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    /**
     * Возвращает delta-time, ссылка на Gdx.graphics.getDeltaTime()
     */
    public static float dt() {
        return Gdx.graphics.getDeltaTime();
    }

    /**
     * Загружает спрайт из asset-ов.
     */
    public static Sprite load(String path) {
        return SL.inst().load(path);
    }

    /**
     * Хранит небольшую базу всех кнопок, и сообщает, только что ли нажата кнопка.
     */

    private static HashMap<Integer, Boolean> keys_state = new HashMap<>();
    public static boolean jkey(int id) {

        if (!key(id)) {
            keys_state.put(id, false);
            return false;
        } else {
            if (keys_state.get(id) == null)
                keys_state.put(id, false);

            if (!keys_state.get(id)) {
                keys_state.put(id, true);
                return true;
            } else
                return false;
        }
    }

    public static void log(Object text) {
        text = text == null ? "null" : text;

        try {
            Gdx.app.log("Luna Log", text.toString());
        } catch (NullPointerException e) {
            System.out.println("Luna Log : " + text.toString());
        }
    }

    public static void log(double... nums) {
        String all = "";
        int len = 20;

        for (double num : nums) {
            String tmp = num + "";
            for (int i = 0; i < len - tmp.length(); i++) tmp += " ";
            all += tmp;
        }

        log(all);
    }

    public static Vector2 mouse(int point) {
        return new Vector2(Gdx.input.getX(point), screen().y - Gdx.input.getY(point));
    }

    public static class CameraLinkParams {
        public Rectangle bounds;
        public GameObj target;
        public float zoom = 1f;
        public float speed = 0.5f;

        public CameraLinkParams(GameObj obj, Rectangle rect) {
            target = obj;
            bounds = rect;
            zoom = 1;
            speed = 0.5f;
        }

        public CameraLinkParams() {}

        public void update() {
        }
    }

    public static GameObj getCameraLink(final CameraLinkParams clp) {
        return new GameObj() {
            Vector2 pos = clp.target.pos.cpy().add(clp.target.size.cpy().div(2));

            @Override
            public void render(SpriteBatch batch) {
            }

            @Override
            public void update(float time) {
                if (clp.target == null || !clp.target.isAlive) {
                    isAlive = false;
                    return;
                }

                Vector2 lbc;
                Vector2 ruc;

                lbc = new Vector2(clp.bounds.x, clp.bounds.y);
                ruc = new Vector2(lbc.cpy().add(clp.bounds.width, clp.bounds.height));

                Vector2 centered = clp.target.pos.cpy().add(clp.target.size.cpy().div(2));
                pos.add(centered.sub(pos).mul(clp.speed));
                clp.update();
                clp.target.parent.gfx.focusOn(pos, lbc, ruc, clp.zoom);
            }
        };
    }

    public static GameObj getLinker(final GameObj target, final GameObj attaching, final Vector2 offset) {
        return new GameObj() {
            @Override
            public void render(SpriteBatch batch) {
            }

            @Override
            public void update(float time) {
                isAlive = target.isAlive && attaching.isAlive;
                if (isAlive) {
                    Vector2 converted_pos = target.pos.cpy().add(offset);
                    converted_pos = target.parent.gfx.convertFromGFX(converted_pos);
                    converted_pos = attaching.parent.gfx.convertToGFX(converted_pos);
                    attaching.pos.set(converted_pos);
                }
            }
        };
    }

    public static <T> T[] array(T... objects) {
        return objects;
    }

}
