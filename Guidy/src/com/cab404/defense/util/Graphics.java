package com.cab404.defense.util;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.Luna;

/**
 * @author cab404
 */
public class Graphics {
    public SpriteBatch batch;
    public OrthographicCamera cam;

    public Graphics() {
        batch = new SpriteBatch();
        cam = new OrthographicCamera(Luna.screen().x, Luna.screen().y);

        update();
    }

    /**
     * Returns camera position
     */
    public Vector2 cam_pos() {
        return convertToGFX(new Vector2(cam.position.x, cam.position.y));
    }

    /**
     * Returns camera size
     */
    public Vector2 cam_size() {
        return new Vector2(cam.viewportWidth, cam.viewportHeight).mul(cam.zoom);
    }

    /**
     * Returns BB of the camera
     */
    public Rectangle getCameraBounds() {
        Vector2 pos = cam_pos();
        Vector2 size = cam_size();
        return new Rectangle(pos.x, pos.y, size.x, size.y);
    }

    /**
     * Настраивает камеру.
     *
     * @param pos  Координаты середины зоны видимости
     * @param lbc  Нижний левый угол зоны видимости
     * @param ruc  Верхний правый угол зоны видимости
     * @param zoom Увеличение камеры - отношение пикселя в зоне видимости к реальному.
     */
    public Vector2 focusOn(Vector2 pos, Vector2 lbc, Vector2 ruc, float zoom) {
        Vector2 point = pos.cpy();
        float hw = cam.viewportWidth / 2 / zoom, hh = cam.viewportHeight / 2 / zoom;
        point.sub(hw, hh);

        point.x = (point.x + hw) < lbc.x + hw ? lbc.x : ((point.x + hw * 2) > ruc.x ? ruc.x - hw * 2 : point.x);
        point.y = (point.y + hh) < lbc.y + hh ? lbc.y : ((point.y + hh * 2) > ruc.y ? ruc.y - hh * 2 : point.y);

        cam.position.set(point.x, point.y, 0);

        cam.zoom = 1f / zoom;
        point.add(hw, hh);
        return point;
    }

    public void update() {
        Vector2 scr = Luna.screen();
        cam.viewportWidth = scr.x;
        cam.viewportHeight = scr.y;
        cam.update();
        batch.setProjectionMatrix(cam.projection);
        // Я не знаю, кагого Дискорда у камеры 0:0 в центре по умолчанию, но нафиг.
        batch.getProjectionMatrix().translate(
                -scr.x / 2 * cam.zoom,
                -scr.y / 2 * cam.zoom,
                0);
        batch.setTransformMatrix(cam.view);
    }

    /**
     * Конвертирует координаты на экране в координаты на заданной данным классом плоскости.
     */
    public Vector2 convertToGFX(Vector2 in) {
        return in.cpy()
                .mul(cam.zoom)
                .add(cam.position.x, cam.position.y)
                ;
    }

    /**
     * Обратно функции convertToGFX
     */
    public Vector2 convertFromGFX(Vector2 in) {
        return in.cpy()
                .sub(cam.position.x, cam.position.y)
                .div(cam.zoom)
                ;
    }

    public Vector2 convertTo(Graphics gfx, Vector2 in) {
        return convertToGFX(gfx.convertFromGFX(in));
    }
}
