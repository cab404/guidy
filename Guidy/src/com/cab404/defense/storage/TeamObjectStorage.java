package com.cab404.defense.storage;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.cab404.defense.objects.GameObj;
import com.cab404.defense.util.Graphics;
import javolution.util.FastList;
import javolution.util.FastMap;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

/**
 * Хранилище для объектов.
 *
 * @author cab404
 */
public class TeamObjectStorage extends AbstractObjectStorage {

    /**
     * А еще у нас будет магическая команда цвета null, на которую ничто не будет агрится.
     */
    private FastMap<Color, Array<GameObj>> commands;

    public TeamObjectStorage() {
        commands = new FastMap<>();
        gfx = new Graphics();
    }

    public void addObjectToCommand(Color command, GameObj... object) {
        for (GameObj obj : object) addObjectToCommand(command, obj);
    }

    /**
     * Добавляет объект в хранилище по флагу команды team, выставляя флаг в stats
     */
    public void addObjectToCommand(Color command, GameObj object) {
        object.parent = this;
        if (!commands.containsKey(command))
            commands.put(command, new Array<GameObj>());
        commands.get(command).add(object);
    }

    public void remove(Color command, GameObj obj) {
        if (commands.containsKey(command)) {
            commands.get(command).removeValue(obj, true);
        }
    }

    public Array<GameObj> getAllTeamsExcept(Color exception) {
        Array<GameObj> all_objects = new Array<>();
        Iterator<Color> keys = commands.keySet().iterator();

        for (Map.Entry<Color, Array<GameObj>> entry : commands.entrySet()) {
            if (!entry.getKey().equals(Color.WHITE) && !entry.getKey().equals(exception))
                all_objects.addAll(entry.getValue());
        }

        return all_objects;

    }

    public Collection<GameObj> getAll() {
        FastList<GameObj> all_objects = new FastList<>();
        for (Array<GameObj> gameObjects : commands.values()) {
            Collections.addAll(all_objects, gameObjects.toArray());
        }
        return all_objects;

    }


    public void add(GameObj... obj) {
        this.addObjectToCommand(Color.WHITE, obj);
    }

    @Override
    public void remove(GameObj... remove) {
        for (Array<GameObj> arr : commands.values()) {
            for (GameObj obj : remove) {
                arr.removeValue(obj, true);
            }
        }
    }
}
