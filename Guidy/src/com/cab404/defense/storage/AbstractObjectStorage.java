package com.cab404.defense.storage;

import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.defense.util.Graphics;
import com.cab404.guidy.Luna;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * @author cab404
 */
public abstract class AbstractObjectStorage implements Iterable<GameObj> {
    public Graphics gfx;

    public abstract void remove(GameObj... remove);
    public void add(GameObj... add) {
        for (GameObj obj : add) obj.parent = this;
    }

    public abstract Collection<GameObj> getAll();

    public AbstractObjectStorage() {
        gfx = new Graphics();
    }

    public void update(int iterations, float time) {
        gfx.update();
        if (time > 0 && iterations > 0)
            for (int i = iterations; i != 0; i--) {
                internalUpdate(time / iterations);
            }
    }

    public void update(float time) {
        gfx.update();
        internalUpdate(time);
    }

    public void internalUpdate(float time) {

        Vector2 mouse = gfx.convertToGFX(Luna.mouse(0));
        ArrayList<GameObj> dead = new ArrayList<>();

        Collection<GameObj> objects = getAll();

        for (GameObj upd : objects) {
            if (upd.isAlive) {

                if (upd.getBounds().contains(mouse.x, mouse.y)) {
                    upd.onMouseOver(mouse.cpy().sub(upd.pos));
                }

                upd.update(time);
            } else {
                upd.onDeath();
                dead.add(upd);
            }
        }

        remove(dead.toArray(new GameObj[dead.size()]));
    }


    public void render() {
        gfx.batch.begin();
        for (GameObj toRender : getAll()) {
            toRender.render(gfx.batch);
        }
        gfx.batch.end();
    }

    @Override public Iterator<GameObj> iterator() {
        return getAll().iterator();
    }
}
