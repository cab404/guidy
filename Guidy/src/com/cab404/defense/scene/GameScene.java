package com.cab404.defense.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.storage.AbstractObjectStorage;
import com.cab404.defense.storage.TeamObjectStorage;
import com.cab404.defense.util.Graphics;
import com.cab404.guidy.Luna;

import java.util.ConcurrentModificationException;
import java.util.Random;

/**
 * Сцена с возможностью прорисовки двух слоёв - игры и UI.
 *
 * @author cab404
 */
public class GameScene extends SimpleScene {

    public AbstractObjectStorage game;
    public Random rnd = new Random();
    public float time_zoom = 1;

    @Override
    public void create() {
        super.create();
        game = new TeamObjectStorage();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    @Override
    public void resize(Vector2 size) {
        super.resize(size);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        game.render();
        objects.render();
    }

    @Override
    public void update() {
        try {
            game.update(1, Gdx.graphics.getDeltaTime() * time_zoom);
        } catch (ConcurrentModificationException ex) {
            throw new RuntimeException(ex);
        }

        super.update();
    }

    public static class ZoomInputProcessor implements InputProcessor {

        public float delta = 0.01f;
        private Graphics gfx;

        public ZoomInputProcessor(Graphics gfx) {
            this.gfx = gfx;
        }

        @Override
        public boolean keyDown(int keycode) {
            return false;
        }


        @Override
        public boolean keyUp(int keycode) {
            return false;
        }


        @Override
        public boolean keyTyped(char character) {
            return false;
        }


        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            return false;
        }


        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            return false;
        }


        private float last_dst = 0;
        private Vector2 last_center;

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            if (Gdx.input.isTouched(0) && Gdx.input.isTouched(1)) {

                Vector2 p1 = Luna.mouse(0);
                Vector2 p2 = Luna.mouse(1);

                Vector2 center = p1.cpy().add(p2).div(2);

                float dst = p1.dst(p2);

                if (last_dst == 0) {
                    last_center = center.cpy();
                    last_dst = dst;
                }

                float zoom_mn = (last_dst / dst - 1);// / cam.zoom;

                Vector2 pos_1 = gfx.convertToGFX(center);
                OrthographicCamera cam = gfx.cam;

                cam.zoom /= 1 - zoom_mn;
                cam.zoom = cam.zoom > delta ? cam.zoom : delta;
                cam.zoom = cam.zoom < 10f ? cam.zoom : 10f;
                Vector2 pos_2 = gfx.convertToGFX(center);

                cam.translate(pos_1.sub(pos_2));
                cam.translate(gfx.convertToGFX(last_center).sub(gfx.convertToGFX(center)));

                last_center = center.cpy();
                last_dst = dst;
            } else {
                last_dst = 0;
            }
            return false;
        }


        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }


        @Override
        public boolean scrolled(int amount) {
            // Довольно просто - сохраняем положение
            // курсора до зума и после, разницу добавляем к положению камеры, и
            // вуаля - курсор при зуме всегда на одной точке.

            Vector2 mouse_pos_1 = gfx.convertToGFX(Luna.mouse(0));
            gfx.cam.zoom += amount * delta;
            gfx.cam.zoom = gfx.cam.zoom > delta ? gfx.cam.zoom : delta;
            Vector2 mouse_pos_2 = gfx.convertToGFX(Luna.mouse(0));


            gfx.cam.translate(mouse_pos_1.sub(mouse_pos_2));

            return true;
        }

    }

}
