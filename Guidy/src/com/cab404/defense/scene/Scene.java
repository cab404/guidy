package com.cab404.defense.scene;

import com.badlogic.gdx.math.Vector2;

/**
 * @author cab404
 */
public interface Scene {
    public void create();
    public void update();
    public void render();
    public void dispose();
    public void resize(Vector2 size);

}
