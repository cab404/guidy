package com.cab404.defense.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.util.FL;
import com.cab404.guidy.Luna;

public abstract class TextField implements Field {

    public abstract String getText();

    /**
     * Расстояние от текста до края фона
     */
    public float frame = 5;//, font_scale = 1f;
    public Sprite bg;
    public int font = FL.DEFAULT;
    public Color color, font_color;


    public TextField() {
        color = Color.DARK_GRAY.cpy();
        color.a = 0.5f;

        font_color = Color.WHITE.cpy();
        font_color.a = 0.7f;

        Pixmap pix = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pix.setColor(Color.WHITE);
        pix.fill();

        bg = new Sprite(new Texture(pix));
    }

    @Override
    public void render(SpriteBatch batch, float phase, Vector2 lbc) {
        Color alpha_bg = color.cpy();
        alpha_bg.a *= phase;

        Color alpha_font = font_color.cpy();
        alpha_font.a *= phase;

        bg.setColor(alpha_bg);
        bg.setPosition(lbc.x + bg.getWidth() * (1f - phase), lbc.y);

        Luna.font(font).setColor(alpha_font);

        String txt = getText();
        TextBounds bounds = Luna.font(font).getMultiLineBounds(txt);

        bg.setSize(bounds.width + frame * 2, bounds.height + frame * 2);
        bg.draw(batch);

        Luna.font(font).drawMultiLine(
                batch,
                txt,
                lbc.x + frame + bg.getWidth() * (1f - phase),
                lbc.y + frame + bounds.height
        );
    }

    @Override
    public Vector2 getSize(float phase) {
        String txt = getText();
//        SpriteStorage.def.setScale(phase * font_scale);
        TextBounds bounds = Luna.font(font).getMultiLineBounds(txt);
        return new Vector2((bounds.width + frame * 2) * phase, (bounds.height + frame * 2) * phase);
    }

    @Override
    public boolean isDead() {
        return false;
    }

    @Override
    public void onMouseOver(Vector2 mouse) {

    }

    @Override
    public void update(float time) {
    }

    public static TextField create(final String staticText, final int font_id) {
        return new TextField() {
            {this.font = font_id;}
            @Override public String getText() {
                return staticText;
            }
        };
    }

}
