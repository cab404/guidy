package com.cab404.defense.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * Интерфейс элементов меню
 *
 * @author cab404
 */
public interface Field {
    /**
     * Рисует поле меню
     *
     * @param lbc Левый нижний угол
     */
    public abstract void render(SpriteBatch batch, float phase, Vector2 lbc);

    public abstract Vector2 getSize(float phase);

    public void onMouseOver(Vector2 mouse_pos);

    public void update(float time);

    public boolean isDead();

}