package com.cab404.defense.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public abstract class SelectableField extends TextField {
    public boolean isDead = false;
    private boolean isMouseOver = false;
    // В принципе, отвечает за то, как будет существовать поле:
    // true :  Поле просуществует до того момента, когда мышку/тачпад перестанут нажимать. Если нажимать перестали
    //         на кнопке (навели-отпустили), то вызывается onSelect. Идеально для мелких in-game меню, или их подуровней.
    // false : Поле действует как обычная кнопка, onSelect вызывается по щелчку.
    public boolean fast_selection = false;

    public abstract void onSelect(Vector2 mouse);

    @Override
    public boolean isDead() {
        return isDead || (fast_selection && !Gdx.input.isTouched());
    }


    @Override
    public void onMouseOver(Vector2 mouse_pos) {
        isMouseOver = true;
        color.a = 0.9f;
        if (fast_selection) {
            if (!Gdx.input.isTouched()) {
                onSelect(mouse_pos);
            }
        } else if (Gdx.input.justTouched()) {
            onSelect(mouse_pos);
        }
    }


    @Override
    public abstract String getText();


    @Override
    public void update(float time) {
        if (!isMouseOver) {
            color.a -= 0.5f * time;
            if (color.a < 0.5f) color.a = 0.5f;
        } else {
            isMouseOver = false;
        }
    }

}
