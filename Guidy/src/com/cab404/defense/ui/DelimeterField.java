package com.cab404.defense.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * @author cab404
 */
public class DelimeterField implements Field {

    private Vector2 size;

    public DelimeterField(Vector2 size) {

        this.size = size;
    }

    public DelimeterField(int w, int h) {
        this(new Vector2(w, h));
    }

    @Override
    public void render(SpriteBatch batch, float phase, Vector2 lbc) {
    }

    @Override
    public Vector2 getSize(float phase) {
        return size.cpy().mul(phase);
    }

    @Override
    public void onMouseOver(Vector2 mouse_pos) {
    }

    @Override
    public void update(float time) {
    }

    @Override
    public boolean isDead() {
        return false;
    }
}
