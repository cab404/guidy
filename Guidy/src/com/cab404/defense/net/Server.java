package com.cab404.defense.net;

import com.cab404.guidy.Luna;
import javolution.util.FastList;
import javolution.util.FastMap;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Простейший сервер. Не уверен, что работает, но, по идее, должен.
 *
 * @author cab404
 */


public class Server {
    public int port = 10100;
    public boolean isRunning = true;
    public FastMap<Long, PlayerListeningConnection> players;
    private FastList<Packet> broadcast;
    private long incrementalID = 0;
    private static JSONParser parser = new JSONParser();

    public Server() {
        players = new FastMap<>();
        broadcast = new FastList<>();
        players.setShared(true);
    }

    public void start() {
        new Thread(new ConnectionListener()).start();
        new Thread(new Broadcast()).start();
    }

    public class ConnectionListener implements Runnable {

        @Override
        public void run() {
            ServerSocket listening = null;
            try {
                listening = new ServerSocket(port);
                Luna.log("Server created. Everything should be just fine. Try connecting to 127.0.0.1:" + port);
                while (isRunning) {
                    PlayerListeningConnection connection;
                    incrementalID++;

                    connection = new PlayerListeningConnection(listening.accept());
                    new Thread(connection).start();
                }
            } catch (IOException e) {
                Luna.log("Server could not be created.");
                Luna.log(e);
                assert listening != null;
                try {
                    listening.close();
                } catch (IOException ex) {
                    Luna.log(ex);
                }
            }
        }

    }

    public class Broadcast implements Runnable {

        @Override
        public void run() {
            while (isRunning) {

                synchronized (broadcast) {
                    // Если нужно отправить больше 3к пакетов - в следующюю итерацию.
                    for (int i = 0; broadcast.size() > 0 && i != 3000; i++) {
                        Packet packet = broadcast.removeLast();

                        for (PlayerListeningConnection player : players.values()) {
                            if (player.id != packet.id) {
                                Luna.log(packet.id + " > " + player.id + " : Packet sent! ");
                                player.out.println(packet.message);
                                player.out.flush();
                            }

                        }

                    }
                }
            }

        }

    }

    public class PlayerListeningConnection implements Runnable {
        public Scanner in;
        public PrintWriter out;
        public Socket socket;
        public String last_packet = "";
        public long id;

        public PlayerListeningConnection(Socket socket) {
            try {

                this.socket = socket;

                in = new Scanner(socket.getInputStream());
                out = new PrintWriter(socket.getOutputStream());

                // Не должно быть никаких проблем с синхронностью,
                // конструктор вызывается только из одного потока.
                id = incrementalID;

                out.println(id);
                for (PlayerListeningConnection player : players.values()) {
                    out.println(player.last_packet);
                }
                out.flush();

                players.put(id, this);
                Luna.log(id + " : Connection accepted");

            } catch (IOException e) {
                Luna.log("Cannot create player listener.");
                Luna.log(e);
            }

        }

        @Override
        public void run() {
            while (!socket.isClosed() && isRunning) {
                while (in.hasNextLine()) {
                    String line = in.nextLine();
                    try {
                        parser.parse(line);

                        last_packet = line;

                        synchronized (broadcast) {
                            broadcast.add(new Packet(id, line));
                            Luna.log("Broadcast < " + id + " : " + line + ", now " + broadcast.size() + " packets.");
                        }

                    } catch (Exception e) {
                        try {
                            players.remove(id);
                            socket.close();
                            break;
                        } catch (IOException ex) {
                            e.printStackTrace();
                        }

                    }
                }
                in.close();
                out.close();

                try {
                    socket.close();
                } catch (IOException e) {
                    Luna.log(id + " : Cannot close socket");
                }

                Luna.log(id + " : Player disconnected");
            }
        }

    }

    public class Packet {
        public long id;
        public String message;

        public Packet(long id, String message) {
            this.id = id;
            this.message = message;
        }
    }


}
