package com.cab404.defense.net;

import com.cab404.guidy.Luna;
import javolution.util.FastMap;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Iterator;
import java.util.Scanner;

/**
 * @author cab404
 */
public class ServerConnection {
    public FastMap<Long, FastMap> players;

    public boolean isUpdated = false;
    public FastMap<Object, Object> self;

    public Socket socket;
    public PrintWriter out;
    public Scanner in;
    private boolean isRunning = true;
    private long id;

    public ServerConnection(String address, int port) {
        players = new FastMap<>();
        self = new FastMap<>();
        try {
            socket = new Socket(InetAddress.getByName(address), port);

            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream());

            String line = in.nextLine();
            id = Long.parseLong(line);
            Luna.log(id + " : " + line);

            self.put(Data.X, 0);
            self.put(Data.Y, 0);
            self.put(Data.ID, id);
            self.put(Data.NAME, "Anonymous");

            isUpdated = true;

            new Thread(new InputUpdater()).start();
            new Thread(new OutputUpdater()).start();


        } catch (IOException e) {
            Luna.log("Cannot connect to server.");
            Luna.log(e);
        }

    }

    private class InputUpdater implements Runnable {

        @Override
        public void run() {
            while (isRunning) {
                while (in.hasNext()) {
                    String line = in.nextLine();
                    Luna.log(line);
                    if (line.equalsIgnoreCase("ping")) {
                        synchronized (out) {
                            out.write("pong");
                            out.flush();
                        }
                    } else {
                        try {
                            FastMap<Object, Object> map = new FastMap<>();

                            JSONParser parser = new JSONParser();
                            JSONObject json = (JSONObject) parser.parse(line);
                            Iterator keys = json.keySet().iterator();
                            Iterator values = json.values().iterator();

                            // Да, переводим вручную. Нечего на меня так смотреть!
                            while (keys.hasNext()) {
                                Object key = keys.next();
                                Object value = values.next();
                                map.put(key, value);
                            }

                            long id = Long.parseLong(map.get("0").toString());
                            if (map.size() == 1) {
                                players.remove(id);
                                Luna.log("Player " + id + " removed.");
                            } else {
                                Luna.log("Player " + id + (map.containsKey(id) ? " updated." : " added."));
                                players.put(id, map);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

            }
            in.close();
            out.close();
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public class OutputUpdater implements Runnable {

        @Override
        public void run() {
            while (!socket.isClosed()) if (isUpdated) {
                synchronized (out) {
                    out.println(JSONObject.toJSONString(self));
                    out.flush();
                }
                isUpdated = false;
            }
        }
    }


    public void dispose() {
        if (socket != null) {
            synchronized (out) {
                out.println("{\"0\":\"" + id + "\"}");
                out.println("exit");
                out.flush();
            }
            isRunning = false;
        }
    }

    public class Data {
        public static final int
                ID = 0,
                NAME = 1,
                X = 2,
                Y = 3,
                DATA_COUNT = 4;

    }


}
