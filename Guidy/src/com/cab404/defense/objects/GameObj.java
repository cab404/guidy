package com.cab404.defense.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.storage.AbstractObjectStorage;

public abstract class GameObj {

    /**
     * Время с последнего кадра.
     */
    public Vector2 pos, size;
    public boolean isAlive = true;
    public AbstractObjectStorage parent;

    /**
     * Прорисовка элемента.
     */
    public abstract void render(SpriteBatch batch);


    /**
     * Обновление элемента.
     */
    public abstract void update(float time);


    /**
     * Вызывается сразу после уничтожения объекта. isDead выставлять вручную не
     * нужно.
     */
    public void onDeath() {
    }

    public Rectangle getBounds() {
        Rectangle box = new Rectangle();

        box.setWidth(size.x);
        box.setHeight(size.y);
        box.setX(pos.x);
        box.setY(pos.y);

        return box;
    }

    public GameObj() {
        pos = new Vector2();
        size = new Vector2();
    }

//
//    public void genExplosion(int particles) {
//        for (int i = 0; i != particles; i++) {
//            Particle prt = new Particle();
//            prt.stats.color = Math.random() > 0.5 ? Color.ORANGE.cpy() : Color.YELLOW.cpy();
//            centerObjectOnSelf(prt);
//
//            prt.var.way = new Vector2((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);
//
//            prt.stats.speed = (float) (stats.speed / 3 * (Math.random() + 0.5f));
//            prt.live = 0.5f;
//            prt.var.way.nor().add(var.way.cpy().mul(2));
//
//            var.parent.add(prt);
//        }
//    }


    public boolean overlaps(GameObj second_element) {
        Vector2 dot = pos.cpy().add(size.cpy().div(2));

        boolean toReturn;
        float x = second_element.pos.x;
        float y = second_element.pos.y;
        float w = second_element.size.x;
        float h = second_element.size.y;

        toReturn = dot.x > x && dot.y > y && dot.x < x + w && dot.y < y + h;

        return toReturn;
    }

//
//    public final void internalUpdate() {
//        var.lifetime += time;
//
//    }


    public void centerObjectOnSelf(GameObj obj) {
        obj.pos = pos.cpy();
        obj.pos.add(new Vector2(
                (size.x - obj.size.x) / 2,
                (size.y - obj.size.y) / 2));
    }

//
//    /**
//     * Плавно поворачивает объект в сторону цели. И это важно :)
//     */
//    public void smoothRotation() {
//
//
//        if (var.target != null && !var.target.var.isDead) {
//
//            float angleTT = var.target.var.pos.cpy() // Копируем позицию цели
//                    .add(var.target.var.halfSize()) // Центруем позицию,
//                            // добавляя пол размера цели
//                    .sub(var.halfSize()) // ... и вычитая пол своего
//                    .sub(var.pos).angle(); // Вычисляем угол
//
//            float angle = var.way.angle();
//            float delta = angle - angleTT;
//
//            // Решение всяких переходов из -180 в +180 и т.д. Работает.
//
//            if (delta > 180)
//                delta -= 360;
//
//            if (delta < -180)
//                delta += 360;
//
//            if (angle != angleTT) {
//
//                if (Math.abs(delta) > stats.rotation_speed * time) {
//                    if (delta < 0) {
//                        var.way.rotate(stats.rotation_speed * time);
//                    } else if (delta > 0) {
//                        var.way.rotate(-stats.rotation_speed * time);
//                    }
//
//                } else {
//                    var.way.rotate(-delta);
//                }
//
//            }
//
//        }
//
//    }


//    /**
//     * Двигает объект в направлении var.way со скоростью var.speed.
//     *
//     * @param kamikaze Если true - объект взорвётся при столкновении с var.target,
//     *                 нанося stats.damage урона.
//     */
//    public void move(boolean kamikaze) {
//        var.way = var.way.nor();
//
//        if (kamikaze && var.target != null && !var.target.var.isDead) {
//            float delta = time * stats.speed;
//            float checkPrecision = 0.5f;
//            float iterations = delta / checkPrecision;
//
//            for (float i = 0; i < delta; i += checkPrecision) {
//                move_iteration(iterations);
//                var.pos.add(var.way.cpy().mul(checkPrecision));
//
//                if (overlaps(var.target)) {
//                    var.target.var.hp -= stats.damage;
//                    var.hp = 0;
//                    break;
//                }
//
//            }
//        } else {
//            float delta = time * stats.speed;
//            float checkPrecision = 0.5f;
//            float iterations = delta / checkPrecision;
//
//            for (float i = 0; i < delta; i += checkPrecision) {
//                move_iteration(iterations);
//                var.pos.add(var.way.cpy().mul(checkPrecision));
//            }
//        }
//
//    }

    public boolean isInRange(GameObj obj, float range) {
        return obj.pos.dst2(pos) <= Math.pow(range, 2);
    }


//    /**
//     * Отправляет все снаряды, которые возможно, в var.target, при этом учитывая
//     * stats.firing_speed
//     */
//    public void sendAllRockets() {
//
//
//        if (var.target != null && !var.target.var.isDead &&
//                isInRange(var.target, stats.firing_range)) {
//            // Если кто-то в радиусе экстерминации - экстерминатус!
//
//            var.gun_cooldown += time;
//
//            float interval = 1f / stats.firing_speed;
//
//            for (; var.gun_cooldown >= interval; var.gun_cooldown -= interval) {
//                GameObj proj = getProjectile();
//                var.createdObjects.add(proj);
//                var.parent.add(proj);
//            }
//        } else {
//            // А иначе - точим ножи на потенциальную жертву :D
//            var.gun_cooldown += time;
//            var.gun_cooldown =
//                    var.gun_cooldown > 1 / stats.firing_speed
//                            ? 1 / stats.firing_speed
//                            : var.gun_cooldown;
//        }
//    }

//
//    /**
//     * В заданном массиве объектов ищет ближайший.
//     *
//     * @param array Массив для поиска
//     * @return Ближайший объект к данному
//     */
//
//    public GameObj findNearestIn(Array<GameObj> array) {
//
//
//        Iterator<GameObj> objects = array.iterator();
//        GameObj nearest = null;
//        float dist_to_nearest = 0;
//
//        while (objects.hasNext()) {
//            GameObj to_check = objects.next();
//
//            if (nearest == null || dist_to_nearest > to_check.var.pos.dst2(var.pos)) {
//                nearest = to_check;
//                dist_to_nearest = to_check.var.pos.dst2(var.pos);
//            }
//        }
//
//
//        if (nearest != null &&
//                isInRange(nearest, stats.firing_range)) {
//            var.target = nearest;
//        }
//
//
//        return nearest;
//
//    }

//
//    /**
//     * Перенаправляет снаряды на новую цель.
//     */
//    public void retargetObjects() {
//
//        for (GameObj createdObject : var.createdObjects) {
//            createdObject.var.target = var.target;
//        }
//
//    }
//

    /**
     * Вызывается в update(), когда курсор находится в пределах var.img
     */
    public void onMouseOver(Vector2 mousePos) {
    }

//
//    /**
//     * Уничтожает ракету, если улетела черезчер далеко.
//     */
//    public void destroyFarRockets() {
//
//        for (GameObj tmp : var.createdObjects) {
//            float range = stats.firing_range * stats.firing_range * 2;
//
//            if (tmp.var.pos.dst2(var.pos) > range) {
//                tmp.var.hp = 0;
//            }
//
//        }
//
//    }


    public String toString() {
        return this.getClass().toString() + ":" + pos.x + ":" + pos.y;
    }
}
