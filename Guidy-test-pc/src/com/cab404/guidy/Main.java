package com.cab404.guidy;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.cab404.defense.scene.SceneManager;
import com.cab404.guidy.tests.MainMenu;

public class Main {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "";
        cfg.useGL20 = false;
        cfg.width = 1000;
        cfg.height = 800;

        Luna.scene = new SceneManager(new MainMenu());
        new LwjglApplication(Luna.scene, cfg);
    }
}
