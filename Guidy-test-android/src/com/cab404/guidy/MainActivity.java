package com.cab404.guidy;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.cab404.defense.scene.SceneManager;
import com.cab404.guidy.tests.MainMenu;

public class MainActivity extends AndroidApplication {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;

        Luna.scene = new SceneManager(new MainMenu());
        initialize(Luna.scene, cfg);
    }
}